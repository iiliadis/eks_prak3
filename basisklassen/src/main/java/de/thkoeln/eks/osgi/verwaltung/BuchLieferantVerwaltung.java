package de.thkoeln.eks.osgi.verwaltung;

import de.thkoeln.eks.osgi.basic.Buch;
import de.thkoeln.eks.osgi.basic.Lieferant;

import java.util.ArrayList;

public interface BuchLieferantVerwaltung {

    /**
    * Erzeugt ein neues Buch-Objekt.
    * * Hierzu muss auf das Interface NummernVerwaltung
    * * zugegriffen werden.
    * *
    * * @param titel Titel des neuen Buches
    * * @param preis Preis des neuen Buches
    * *
    * * @return Registrierungsnummer des neuen Buches
    * */
    public int neuesBuch(String titel, int preis);

    /**
    * * Liefert das Buch-Objekt zu einer gegebenen Buch-Nummer
    * *
    * * @param buchregnr Nummer des gesuchten Buches
    * *
    * * @return Buch-Objekt zur Nummer buchregnr
    * */
    public Buch getBuch(int buchregnr);

    /**
    * * Erzeugt einen neuen Lieferanten.
    * * Hierzu muuss auf das Interface NummernVerwaltung
    * * zugegriffen werden.
    * *
    * * @param name Name des neuen Lieferanten
    * * @param adresse Adresse des neuen Lieferanten
    * *
    * * @return Nummer des neuen Lieferanten
    * */
    public int neuerLieferant(String name, String adresse);

    /**
    * * Durch diese Methode wird angegeben, dass ein Lieferant
    * * ein bestimmtes Buch liefert.
    * *
    * * @param lief Lieferant, der das Buch liefert
    * * @param buch Buch, welches der Lieferant liefert
    * *
    * */
    public void liefertBuch(Lieferant lief, Buch buch);

    /**
    * * Liefert das Lieferanten-Objekt zu einer
    * * gegebenen Lieferantennummer
    * *
    * * @param lieferantennr Nummer des gesuchten Lieferanten
    * *
    * * @return Lieferanten-Objekt zur Nummer lieferantennr
    * */
    public Lieferant getLieferant(int lieferantennr);

    /**
    * * Liefert alle Bücher, die ein gegebener Lieferant liefert.
    * *
    * * @param lief Lieferant, für den alle gelieferten Bücher
    * * gesucht werden sollen
    * *
    * * @return Liste der vom Lieferanten lief gelieferten Bücher
    * */
    public ArrayList<Buch> alleLieferungen(Lieferant lief);
}
