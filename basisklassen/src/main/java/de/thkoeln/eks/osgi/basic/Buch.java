package de.thkoeln.eks.osgi.basic;

/**
 * Buch mit eindeutiger Registriernummer.
 */
public class Buch {

    private int registrierungsnummer;
    private String titel;
    private int preis;

    public Buch() {}

    public int getRegistrierungsnummer() {
        return registrierungsnummer;
    }

    public void setRegistrierungsnummer(int registrierungsnummer) {
        this.registrierungsnummer = registrierungsnummer;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public int getPreis() {
        return preis;
    }

    public void setPreis(int preis) {
        this.preis = preis;
    }
}
