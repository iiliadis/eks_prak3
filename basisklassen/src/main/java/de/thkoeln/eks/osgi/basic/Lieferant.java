package de.thkoeln.eks.osgi.basic;

import java.util.ArrayList;

/**
 * Lieferant mit eindeutiger Lieferantennummer.
 */
public class Lieferant {

    private int lieferantennr;
    private String name;
    private String adresse;
    private ArrayList<Integer> liefert;

    public Lieferant() {}

    public int getLieferantennr() {
        return lieferantennr;
    }

    public void setLieferantennr(int lieferantennr) {
        this.lieferantennr = lieferantennr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public ArrayList<Integer> getLiefert() {
        return liefert;
    }

    public void setLiefert(ArrayList<Integer> liefert) {
        this.liefert = liefert;
    }
}
