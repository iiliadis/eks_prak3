package de.thkoeln.eks.osgi.zentralverwaltung;

public interface NummernVerwaltung {

    /**
     * Liefert eine neue, eindeutige Buch-Registrierungsnummer
     *
     * @return neue, eindeutige Registrierungsnummer
     */
    public int getNeueBuchRegNr();

    /**
     * Liefert eine neue, eindeutige Lieferantennummer
     *
     * @return neue, eindeutige Lieferantennummer
     */
    public int getNeueLieferantenNr();
}
