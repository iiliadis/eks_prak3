package de.thkoeln.eks.anwendung;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Anwendung implements BundleActivator {
    @Override
    public void start(BundleContext context) {
        //buchVerwService = Implementierung des Interface BuchLieferantVerwaltung“
/*
        int buch1regnr = buchVerwService.neuesBuch("SE", 20);
        System.out.println("Registrierungsnummer von Buch SE: " + buch1regnr);

        int buch2regnr = buchVerwService.neuesBuch("FSA", 120);
        System.out.println("Registrierungsnummer von Buch FSA: " + buch2regnr);

        int buch3regnr = buchVerwService.neuesBuch("EKS", 470);
        System.out.println("Registrierungsnummer von Buch EKS: " + buch3regnr);

        System.out.println();

        int liefer1 = buchVerwService.neuerLieferant("Peter", "Koeln");
        System.out.println("Lieferantennummer von Peter: " + liefer1);

        int liefer2 = buchVerwService.neuerLieferant("Klaus", "Mainz");
        System.out.println("Lieferantennummer von Klaus: " + liefer2);

        System.out.println();

        Lieferant lieferant1 = buchVerwService.getLieferant(liefer1);
        System.out.println("Lieferantenobjekt zu Nummer von Peter gefunden.");

        Lieferant lieferant2 = buchVerwService.getLieferant(liefer2);
        System.out.println("Lieferantenobjekt von Nummer von Klaus gefunden.");

        System.out.println();

        Buch buch1 = buchVerwService.getBuch(buch1regnr);
        Buch buch2 = buchVerwService.getBuch(buch2regnr);
        Buch buch3 = buchVerwService.getBuch(buch3regnr);

        buchVerwService.liefertBuch(lieferant1, buch1);
        System.out.println("Peter liefert Buch SE.");

        buchVerwService.liefertBuch(lieferant1, buch2);
        System.out.println("Peter liefert Buch FSA.");

        buchVerwService.liefertBuch(lieferant2, buch3);
        System.out.println("Klaus liefert Buch EKS.");

        System.out.println();

        ArrayList<Buch> allebuecher1 = buchVerwService.alleLieferungen(lieferant1);

        System.out.println("Alle gefundenen Buecher von Peter:");

        for (
                Buch b : allebuecher1) {
            System.out.print(b.getTitel() + ", " +
                    b.getRegistrierungsnummer());
            System.out.println();
        }

        System.out.println();

        ArrayList<Buch> allebuecher2 = buchVerwService.alleLieferungen(lieferant2);
        System.out.println("Alle gefundenen Buecher von Klaus:");

        for (Buch b : allebuecher2) {
            System.out.print(b.getTitel()+", "+b.getRegistrierungsnummer());
            System.out.println();
        }

        System.out.println();
    }

    public void stop(BundleContext context){
        System.out.println("EKS Prak wurde beendet.");
    }

 */
    }

    @Override
    public void stop(BundleContext context){
        System.out.println("Goodbye from EKS_Prak_03 :)");
    }
}